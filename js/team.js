
document.addEventListener('DOMContentLoaded', function() {
    var teamContainer = document.querySelector('.team_container .row');
    
    var teamMembers = [
        
    {
        name: 'René Walter',
        linkedin: 'https://www.linkedin.com/in/rené-walter-7b38b7165',
        imgSrc: 'images/profilepics/Rene.jpg'
    },
    {
        name: 'Enver Gantemirov',
        linkedin: 'https://www.linkedin.com/in/enver-gantemirov-4814a2252/',
        imgSrc: 'images/profilepics/enver.jpeg'
    },
    {
        name: 'Eyyüp Cetinkaya',
        linkedin: 'https://www.linkedin.com/in/eyyüp-c-064788238',
        imgSrc: 'images/profilepics/Eyyüp.png'
    },
    {
        name: 'Harkirat Singh',
        linkedin: 'https://www.linkedin.com/in/harkirat-singh-88218b187/',
        imgSrc: 'images/profilepics/Harry.png'
    },
    {
        name: 'Marcus Orak',
        linkedin: 'https://www.linkedin.com/in/marcus-orak-39120b24b/',
        imgSrc: 'images/profilepics/marcus.jpg'
    },
    {
        name: 'Matthias Lobenhofer',
        linkedin: 'https://www.linkedin.com/in/matthias-lobenhofer-0b195929b/',
        imgSrc: 'images/profilepics/Matze.jpg'
    }
    ];
    
teamMembers.sort(function() { return 0.5 - Math.random() });

for (var i = 0; i < teamMembers.length; i++) {
    var member = teamMembers[i];
    var col = document.createElement('div');
    col.classList.add('col-lg-3', 'col-sm-6');

    var memberBox = document.createElement('div');
    memberBox.classList.add('box');

    var imgBox = document.createElement('div');
    imgBox.classList.add('img-box');

    var img = document.createElement('img');
    img.src = member.imgSrc;
    img.alt = member.name;

    imgBox.appendChild(img);

    var detailBox = document.createElement('div');
    detailBox.classList.add('detail-box');

    var name = document.createElement('h5');
    name.textContent = member.name;

    var role = document.createElement('p');
    role.textContent = 'Computer Science - Student';

    detailBox.appendChild(name);
    detailBox.appendChild(role);

    var socialBox = document.createElement('div');
    socialBox.classList.add('social_box');

    var linkedinLink = document.createElement('a');
    linkedinLink.href = member.linkedin;
    linkedinLink.target = '_blank';

    var linkedinIcon = document.createElement('i');
    linkedinIcon.classList.add('fa', 'fa-linkedin');
    linkedinIcon.setAttribute('aria-hidden', 'true');

    linkedinLink.appendChild(linkedinIcon);
    socialBox.appendChild(linkedinLink);

    memberBox.appendChild(imgBox);
    memberBox.appendChild(detailBox);
    memberBox.appendChild(socialBox);

    col.appendChild(memberBox);

    // Check if it's the 5th or 6th member and add a class for styling
    if (i === 4 || i === 5) {
        col.classList.add('centered');
    }

    teamContainer.appendChild(col);
}
    });
